//
//  ReviewsRequest.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

import Foundation

class ReviewsRequest<T: Decodable>: ReactiveRequest {
    typealias argType = T
    
    var method = RequestType.GET
    var path = ""
    var parameters = [String: String]()
    
    func parse(data: Data) -> T? {
//        do {
            let decoder = JSONDecoder()
            return try? decoder.decode(T.self, from: data)
//        } catch {
//            fatalError(error.localizedDescription)
//        }
    
    }

    init(path: String, parameters: [String: String] = [String: String]()) {
        self.path = path
        self.parameters = parameters
    }
}

enum ReviewsSortOrder: String {
    case ratingAscending = "Rating (lowest first)"
    case ratingDescending = "Rating (highest first)"
    case dateAscending = "Oldest first"
    case dateDescending = "Newest first"
    
    var params: [String: String] {
        switch self {
        case .ratingAscending:
            return ["sortBy": "rating", "direction": "asc"]
        case .ratingDescending:
            return ["sortBy": "rating", "direction": "desc"]
        case .dateAscending:
            return ["sortBy": "date_of_review", "direction": "asc"]
        case .dateDescending:
            return ["sortBy": "date_of_review", "direction": "desc"]
        }
    }
}
