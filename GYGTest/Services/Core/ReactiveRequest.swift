//
//  ReactiveRequest.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

import Foundation

public enum RequestType: String {
    case GET, POST, PUT
}

protocol ReactiveRequest {
    associatedtype argType
    var method: RequestType { get }
    var path: String { get }
    var parameters: [String : String] { get }
    func parse(data: Data) -> argType?
}

extension ReactiveRequest {
    
    func request(with baseURL: URL) -> URLRequest {
        var components: URLComponents?
        
        if let path = self.path.removingPercentEncoding?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
            if path.starts(with: "http"), let url = URL(string: path) {
                components = URLComponents(url: url, resolvingAgainstBaseURL: false)
            }
            else if let newUrl = URL(string: baseURL.absoluteString + path) {
                components = URLComponents(url: newUrl, resolvingAgainstBaseURL: false)
            }
            else {
                components = URLComponents(url: baseURL.appendingPathComponent(path), resolvingAgainstBaseURL: false)
            }
        }
        
        if var components = components {
            if components.queryItems == nil || components.queryItems!.count == 0 {
                components.queryItems = parameters.count > 0 ? parameters.map {
                    URLQueryItem(name: String($0), value: String($1))
                    } : nil
            } else {
                components.queryItems = components.queryItems! + parameters.map {
                    URLQueryItem(name: String($0), value: String($1))
                } 
            }
            guard let url = components.url else {
                fatalError("Could not get url")
            }
            
            var request = URLRequest(url: url)
            request.httpMethod = method.rawValue
            return request
        }
        fatalError("Unable to create URL components")
    }
}
