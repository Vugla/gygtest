//
//  ReactiveClient.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

import RxSwift

class ReactiveClient {
    //  MARK: - Singleton
    static let shared = ReactiveClient(baseUrl: URL(string: PlistManager.getValue(for: PlistKeys.apiUrl) as! String)!)
    
    //  MARK: - Properties
    let domainName = "GYG"
    let defaultErrorCode = 600
    private var baseURL: URL
    
    //  MARK: Initialization
    init(baseUrl: URL) {
        self.baseURL = baseUrl
    }
    
    //  MARK: Network
    func send<T: ReactiveRequest>(apiRequest: T) -> Observable<T.argType?> {
        return Observable<T.argType?>.create { observer in
            let request = apiRequest.request(with: self.baseURL)
            let task = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
                guard let self = self else { return }
                if !PlistManager.isProduction() {
                    self.printData(data, request: request, response: response)
                }
                if let error = error {
                    observer.onError(error)
                } else if let model: T.argType = apiRequest.parse(data: data ?? Data()) {
                    observer.onNext(model)
                    observer.onCompleted()
                } else {
                    observer.onError(NSError(domain: self.domainName, code: self.defaultErrorCode, userInfo: [NSLocalizedDescriptionKey: "Parsing Error."]))
                }
            }
            task.resume()
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
    
    //  MARK: - Utility
    private func printData(_ data: Data?, request: URLRequest?, response: URLResponse?) {
        let datastring = NSString(data: data ?? Data(), encoding: String.Encoding.utf8.rawValue)
        print("\nRequest headers: \(request?.allHTTPHeaderFields ?? ["": "- Empty -"])")
        print("Request: 🔗 \(response?.url?.absoluteString ?? "- Empty -") 🔗")
        print("Response body: 📦 \(datastring ?? " - Empty - ") 📦\n")
    }
}
