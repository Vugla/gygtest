//
//  PlistManager.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

import Foundation

struct PlistKeys {
    static let apiUrl = "apiUrl"
}

class PlistManager {
    
    private static var configPlistDictionary: NSDictionary? {
        get {
            return NSDictionary(contentsOfFile: Bundle.main.path(forResource: "Config", ofType: "plist")!)
        }
    }
    
    private static var developmentDictionary: NSDictionary? {
        get {
            guard let dictionary = configPlistDictionary, let developmentDictionary = dictionary["Development"] as? NSDictionary else {
                return nil
            }
            return developmentDictionary
        }
    }
    
    private static var productionDictionary: NSDictionary? {
        get {
            guard let dictionary = configPlistDictionary, let productionDictionary = dictionary["Production"] as? NSDictionary else {
                return nil
            }
            
            return productionDictionary
        }
    }
    
    static func isProduction() -> Bool {
        guard let dictionary = configPlistDictionary, let enviroment = dictionary["enviroment"] as? String else {
            return false
        }
        
        return enviroment.lowercased() == "production"
    }
    
    static func getValue(for key: String) -> Any? {
        if isProduction() {
            return productionDictionary?[key]
        }
        return developmentDictionary?[key]
    }
}
