//
//  WMModalPickerView.swift
//  Telemach
//
//  Created by Predrag Samardzic on 12/15/16.
//  Copyright © 2016 Predrag Samardzic. All rights reserved.
//

import UIKit

class WMModalPickerView: WMModalPickerBase {
    
    var values = Array<String>()
    internal var indexSelectedBeforeDismissal:Int?
    var selectedIndex:Int = 0
    var selectedValue:String? {
        didSet {
            if let value = selectedValue, let index = values.firstIndex(of: value) {
                selectedIndex = index
            }
        }
    }
    
    init(values: [String]) {
        super.init(frame: CGRect.zero)
        self.values = values
        self.isUserInteractionEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func pickerWithFrame(pickerFrame: CGRect) -> UIView {
        let pickerView = UIPickerView.init(frame: pickerFrame)
        pickerView.dataSource = self
        pickerView.delegate = self
        pickerView.showsSelectionIndicator = true
        pickerView.selectRow(selectedIndex, inComponent:0, animated:false)
        indexSelectedBeforeDismissal = selectedIndex
        return pickerView
    }
    
    override func onDone(sender: AnyObject) {
        if let indexBeforeDismiss = self.indexSelectedBeforeDismissal {
            self.selectedIndex = indexBeforeDismiss
            if values.count > selectedIndex {
                selectedValue = values[selectedIndex]
            }
        }
        super.onDone(sender: sender)
    }
}

extension WMModalPickerView: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return values.count
    }
    
    //  Picker View Delegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.values[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        indexSelectedBeforeDismissal = row
    }
}
