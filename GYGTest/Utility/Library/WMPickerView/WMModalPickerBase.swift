//
//  WMModalPickerBase.swift
//  Telemach
//
//  Created by Predrag Samardzic on 12/15/16.
//  Copyright © 2016 Predrag Samardzic. All rights reserved.
//

import UIKit

class WMModalPickerBase: UIView {
    
    typealias WMModalPickerViewCallback = (_ madeChoice: Bool) -> Void
    
    override var tintColor: UIColor! {
        didSet {
            cancelButton.tintColor = tintColor
            cancelButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor:tintColor!], for: .normal)
            doneButton.tintColor = tintColor
            doneButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor:tintColor!], for: .normal)
        }
    }
    
    private let cancelButton = UIBarButtonItem(title: Strings.cancel, style: .done, target: self, action: #selector(onCancel))
    private let doneButton = UIBarButtonItem(title: Strings.confirm, style: .done, target: self, action: #selector(onDone))
    private let PICKER_HEIGHT:CGFloat = 260
    private let TOOLBAR_HEIGHT:CGFloat = 44
    
    var panel = UIView(frame: CGRect.zero)
    var picker: UIView {
        let pickerFrame = CGRect(x:0, y:TOOLBAR_HEIGHT, width:self.bounds.size.width, height:PICKER_HEIGHT - TOOLBAR_HEIGHT)
        let ret = pickerWithFrame(pickerFrame: pickerFrame)
        ret.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        ret.backgroundColor = UIColor.white
        return ret
    }
    
    var toolbar: UIToolbar {
        let ret = UIToolbar(frame: CGRect(x:0, y:0, width:self.bounds.size.width, height:TOOLBAR_HEIGHT))
        ret.barStyle = .default
        
        var toolbarItems = [UIBarButtonItem]()
        toolbarItems.append(fixedSpace())
        toolbarItems.append(cancelButton)
        toolbarItems.append(contentsOf: additionalToolbarItems())
        toolbarItems.append(doneButton)
        toolbarItems.append(fixedSpace())
        ret.items = toolbarItems
        ret.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        
        return ret
    }
    
    var backdropView: UIView {
        let ret = UIView(frame: self.bounds)
        ret.backgroundColor = UIColor.black
        ret.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        ret.alpha = 0.3
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(onBackdropTap))
        ret.addGestureRecognizer(tapRecognizer)
        
        return ret
    }
    /* Determines whether to display the opaque backdrop view.  By default, this is YES. */
    var presentBackdropView = true
    private var callbackBlock:WMModalPickerViewCallback?
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        autoresizesSubviews = true
        presentBackdropView = true
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func pickerWithFrame(pickerFrame:CGRect) -> UIView {
        fatalError("This must be overriden in subclass")
    }
    
    func additionalToolbarItems() -> [UIBarButtonItem] {
        return [UIBarButtonItem(barButtonSystemItem:.flexibleSpace , target: nil, action: nil)]
    }
    
    func fixedSpace() -> UIBarButtonItem {
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        fixedSpace.width = 10
        return fixedSpace
    }
    
    //Event handlers
    
    @objc func onCancel(sender:AnyObject) {
        self.callbackBlock?(false)
        self.dismissPicker()
    }
    
    @objc func onDone(sender:AnyObject) {
        self.callbackBlock?(true)
        self.dismissPicker()
    }
    
    @objc func onBackdropTap(sender:AnyObject) {
        self.onCancel(sender: sender)
    }
    
    func presentInView(view:UIView, callback:@escaping WMModalPickerViewCallback) {
        self.frame = view.bounds
        self.callbackBlock = callback
        
        self.panel.removeFromSuperview()
        self.backdropView.removeFromSuperview()
        
        self.panel = UIView(frame: CGRect(x:0, y:self.bounds.size.height - PICKER_HEIGHT, width:self.bounds.size.width, height:PICKER_HEIGHT))
        self.panel.autoresizesSubviews = true;
        self.panel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        if (self.presentBackdropView) {
            self.addSubview(backdropView)
        }
        
        var rect = self.picker.frame
        rect.size.width = self.panel.frame.size.width
        self.picker.frame = rect
        self.panel.addSubview(self.picker)
        self.panel.addSubview(self.toolbar)
        
        self.addSubview(self.panel)
        view.addSubview(self)
        
        let oldFrame = self.panel.frame
        var newFrame = self.panel.frame
        newFrame.origin.y += newFrame.size.height
        self.panel.frame = newFrame;
        
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: { 
            self.panel.frame = oldFrame
            self.backdropView.alpha = 1
        }, completion: { (finished) in })
    }
    
    func presentInWindowWithBlock(callback: @escaping WMModalPickerViewCallback) {
        let appDelegate = UIApplication.shared.delegate
        let window = appDelegate?.window!
        presentInView(view: window!, callback: callback)
    }
    
    func dismissPicker() {
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {
            var newFrame = self.panel.frame
            newFrame.origin.y += self.panel.frame.size.height
            self.panel.frame = newFrame
            self.backdropView.alpha = 0
        }) { (finished) in
            self.picker.removeFromSuperview()
            self.panel.removeFromSuperview()
            self.backdropView.removeFromSuperview()
            
//            self.picker = nil
//            self.panel = nil
//            self.backdropView = nil
            
            self.removeFromSuperview()
        }
    }
    
}
