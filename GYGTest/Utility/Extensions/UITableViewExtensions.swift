//
//  UITableViewExtensions.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

import UIKit

extension UITableView {
    func setupRefreshControl(refreshControl: UIRefreshControl, activityIndicator: UIActivityIndicatorView, selector: Selector, sender: UIViewController) {
        let tableViewController = UITableViewController()
        
        tableViewController.tableView = self
        refreshControl.tintColor = UIColor.clear
        activityIndicator.clipsToBounds = true
        refreshControl.addSubview(activityIndicator)
        refreshControl.addTarget(sender, action: selector, for: .valueChanged)
        tableViewController.refreshControl = refreshControl
    }
    
    func reloadWithoutAnimation() {
        let lastScrollOffset = contentOffset
        beginUpdates()
        endUpdates()
        layer.removeAllAnimations()
        setContentOffset(lastScrollOffset, animated: false)
    }
}
