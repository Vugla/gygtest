//
//  UINavigationControllerExtensions.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

import Foundation
import UIKit
//
extension UINavigationController {
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .default
    }
}
