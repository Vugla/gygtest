//
//  UIScrollViewExtensions.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

import UIKit

extension UIScrollView {
    func setScrollIndicatorColor(color: UIColor, removeTransparency: Bool = false) {
        for view in subviews {
            if let imageView = view as? UIImageView, let image = imageView.image  {
                imageView.tintColor = color
                imageView.image = image.withRenderingMode(.alwaysTemplate)
                if removeTransparency {
                    imageView.backgroundColor = color
                }
            }
        }
    }
}
