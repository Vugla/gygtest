//
//  UIViewControllerExtensions.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func topController() -> UIViewController {
        if let presentedVC = presentedViewController {
            return presentedVC.topController()
        }
        else if let split = self as? UISplitViewController, let last = split.viewControllers.last {
            return last.topController()
        }
        else if let nav = self as? UINavigationController, let top = nav.topViewController {
            return top.topController()
        }
        else if let tab = self as? UITabBarController {
            if let selected = tab.selectedViewController {
                return selected.topController()
            }
        }
        return self
    }
}
