//
//  StringExtensions.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

import UIKit

//  MARK: - Messages
struct Strings {
    //  Utility
    static let tryAgain = NSLocalizedString("Try again", comment: "")
    static let cancel = NSLocalizedString("Cancel", comment: "")
    static let confirm = NSLocalizedString("Confirm", comment: "")
    
    //  Errors
    static let errorConnection = NSLocalizedString("No internet", comment: "")
    static let errorServer = NSLocalizedString("Server error", comment: "")
    static let noData = NSLocalizedString("No data", comment: "")
    static let genericError = NSLocalizedString("Error", comment: "")
    
    //  Reviews
    static let anonymous = NSLocalizedString("Anonymous", comment: "")
    static let reviews = NSLocalizedString("Reviews", comment: "")
    
    //  Ratings
    static let showAllRatings = NSLocalizedString("All ratings", comment: "")
    static let oneStarRating = NSLocalizedString("%@ star rating", comment: "")
    static let moreStarsRating = NSLocalizedString("%@ stars rating", comment: "")

}

