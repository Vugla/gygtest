//
//  UIViewExtensions.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

import UIKit

var associateObjectValue: Int = 0
// MARK: - UIView Extension
extension UIView {
    
    private var isAnimate: Bool {
        get {
            return objc_getAssociatedObject(self, &associateObjectValue) as? Bool ?? false
        }
        set {
            return objc_setAssociatedObject(self, &associateObjectValue, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    @IBInspectable var shimmerAnimation: Bool {
        get {
            return isAnimate
        }
        set {
            self.isAnimate = newValue
        }
    }
    
    func subviewsRecursiveAnimatable() -> [UIView] {
        return subviews.filter({$0.isAnimate}) + subviews.flatMap { $0.subviewsRecursiveAnimatable() }
    }
    
    func startShimmerAnimation() {
        for animateView in subviewsRecursiveAnimatable() {
            animateView.clipsToBounds = true
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [UIColor.clear.cgColor, UIColor.white.withAlphaComponent(0.8).cgColor, UIColor.clear.cgColor]
            gradientLayer.startPoint = CGPoint(x: 0.7, y: 1.0)
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
            gradientLayer.frame = animateView.bounds
            animateView.layer.mask = gradientLayer
            
            let animation = CABasicAnimation(keyPath: "transform.translation.x")
            animation.duration = 2
            animation.fromValue = -animateView.frame.size.width
            animation.toValue = animateView.frame.size.width
            animation.repeatCount = .infinity
            
            gradientLayer.add(animation, forKey: "")
        }
    }
    
    func stopShimmerAnimation() {
        for animateView in subviewsRecursiveAnimatable() {
            animateView.layer.removeAllAnimations()
            animateView.layer.mask = nil
        }
    }
}

//  MARK: - Modals
extension UIView {
    
    //  MARK: Error
    static let errorTag = 696969
    
    func showError(type: ErrorType, delegate: ErrorViewDelegate?, backgroundColor: UIColor? = .clear) {
        hideError()
        
        let errorView = ErrorView.view(for: type, backgroundColor: backgroundColor, delegate: delegate)
        errorView.tag = UIView.errorTag
        errorView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(errorView)
        bringSubviewToFront(errorView)
        
        
        if #available(iOS 11.0, *) {
            errorView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
            errorView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
            errorView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor).isActive = true
            errorView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor).isActive = true
        }
        else {
            errorView.topAnchor.constraint(equalTo: topAnchor).isActive = true
            errorView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
            errorView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            errorView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        }
    }
    
    func hideError() {
        guard let errorView = viewWithTag(UIView.errorTag) else { return }
        errorView.removeFromSuperview()
    }
    
    //  MARK: Activity Indicator
    func showActivityIndicator(_ style: UIActivityIndicatorView.Style = .gray) {
        hideActivityIndicator()
        let activityIndicator = UIActivityIndicatorView(style: style)
        activityIndicator.color = UIColor(named: "gygColor")
        activityIndicator.tag = 7777
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(activityIndicator)
        bringSubviewToFront(activityIndicator)
        
        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    
        activityIndicator.heightAnchor.constraint(equalToConstant: 25).isActive = true
        activityIndicator.widthAnchor.constraint(equalToConstant: 25).isActive = true
        bringSubviewToFront(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator() {
        viewWithTag(7777)?.removeFromSuperview()
    }

}
