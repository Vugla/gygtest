//
//  ErrorView.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

import UIKit
import RxSwift

protocol ErrorViewDelegate: class {
    func tryAgain()
}

enum ErrorType {
    
    case noNet
    case server
    case noData
    
    var message: String {
        switch self {
        case .noNet:
            return Strings.errorConnection
        case .server:
            return Strings.errorServer
        case .noData:
            return Strings.noData
        }
    }
}

class ErrorView: UIView {

    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var tryAgainButton: UIButton!
    
    private let bag = DisposeBag()
    private weak var delegate: ErrorViewDelegate?
    
    static func view(for type: ErrorType, backgroundColor: UIColor? = .clear, delegate: ErrorViewDelegate?) -> ErrorView {
        let view = Bundle.main.loadNibNamed("ErrorView", owner: self, options: nil)?.first as! ErrorView
        
        view.delegate = delegate
        
        view.backgroundColor = backgroundColor
        view.tryAgainButton.isHidden = delegate == nil
        view.tryAgainButton.setTitle(Strings.tryAgain.uppercased(), for: .normal)
        
        var title: String?
        var image: UIImage?
        switch type {
        case .noNet:
            title = Strings.errorConnection
            image = UIImage(named: "NoNetError")
        case .server:
            title = Strings.errorServer
            image = UIImage(named: "ServerError")
        case .noData:
            title = Strings.noData
            image = UIImage(named: "NoDataError")
        }
        
        view.topLabel.text = title
        view.topLabel.isHidden = title.isNilOrEmpty
        view.imageView.image = image
        view.imageView.isHidden = image == nil
        
        return view
    }
    
    @IBAction func tapOnTryAgain(_ sender: Any) {
        delegate?.tryAgain()
    }
}
