//
//  Review.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

struct Review: Codable {
    
    let review_id: Int?
    let rating: String?
    let title: String?
    let message: String?
    let author: String?
    let foreignLanguage: Bool
    let date: String?
    let languageCode: String?
    let traveler_type: String?
    let reviewerName: String?
    let reviewerCountry: String?
    let reviewerProfilePhoto: String?
    let isAnonymous: Bool
    let firstInitial: String
    
}

extension Review {
    var reviewInfo: String? {
        var info = ""
        if let author = author, !author.isEmpty {
            info = author
        } else {
            info = Strings.anonymous
        }
        
        if isAnonymous {
            info = Strings.anonymous
        }
        
        if let date = date, !date.isEmpty {
            if info.isEmpty {
                info = date
            } else {
                info += " · \(date)"
            }
        }
        return info
    }
}
