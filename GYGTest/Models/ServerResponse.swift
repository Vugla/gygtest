//
//  ServerResponse.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

struct ServerResponse<T: Codable>: Codable {
    
    let status: Bool
    let total_reviews_comments: Int?
    let data: [T]?
    let message: String?
    
}
