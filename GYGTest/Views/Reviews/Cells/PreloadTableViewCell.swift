//
//  PreloadTableViewCell.swift
//  Mondo
//
//  Created by Predrag Samardzic on 12/02/2019.
//  Copyright © 2019 Wireless Media. All rights reserved.
//

import UIKit
import RxSwift

class PreloadTableViewCell: UITableViewCell {

    //  MARK: - Constants
    static let reuseIdentifier = "PreloadTableViewCell"
    
    //  MARK: - Properties
    private let disposeBag = DisposeBag()
    
    //  MARK: - View life cycle
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.startShimmerAnimation()
    }
    
    //  MARK: - Factory
    static func cell(in tableView:UITableView, at indexPath:IndexPath) -> PreloadTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! PreloadTableViewCell
        return cell
    }

}
