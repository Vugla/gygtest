//
//  ReviewTableViewCell.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

import UIKit
import RxSwift

class ReviewTableViewCell: UITableViewCell {
    //  MARK: - Constants
    static let reuseIdentifier = "ReviewTableViewCell"
    
    //  MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var starsStackView: UIStackView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    //  MARK: - Properties
    private var disposables: [Disposable] = [Disposable]()

    //  MARK: - View life cycle
    deinit {
        disposables.forEach{$0.dispose()}
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposables.forEach{$0.dispose()}
    }
    
    //  MARK: - Factory
    static func cell(for review: Review, in tableView: UITableView, at indexPath: IndexPath) -> ReviewTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ReviewTableViewCell
        
        //top label
        cell.titleLabel.text = review.title?.decodingHTMLEntities()
        cell.titleLabel.isHidden = review.title.isNilOrEmpty
        //middle label
        cell.messageLabel.text = review.message?.decodingHTMLEntities()
        cell.messageLabel.isHidden = review.message.isNilOrEmpty
        //bottom label
        cell.infoLabel.text = review.reviewInfo?.decodingHTMLEntities()
        cell.infoLabel.isHidden = review.reviewInfo.isNilOrEmpty
         //rating
        if let ratingString = review.rating, let ratingDouble = Double(ratingString) {
            let rating = Int(ratingDouble)
            cell.starsStackView.isHidden = rating <= 0
            cell.starsStackView.arrangedSubviews.enumerated().forEach{$0.element.alpha = $0.offset < rating ? 1 : 0.2}
        }
        
        return cell
    }

}
