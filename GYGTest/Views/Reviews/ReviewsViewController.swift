//
//  ReviewsViewController.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

import UIKit
import RxSwift

class ReviewsViewController: UIViewController {

    //  MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    
    //  MARK: - Properties
    private var refreshControl = UIRefreshControl()
    private let viewModel = ReviewsViewModel(path: "berlin-l17/tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776/reviews.json")
    private let disposeBag = DisposeBag()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //  MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        setBindings()
    }
    
    //  MARK: - Utility
    private func setupSubviews() {
        navigationController?.navigationBar.shadowImage = UIImage()
        setupRefreshControl()
    }
    
    private func setBindings() {
        //navigation title
        viewModel.numberOfReviews
            .map{[weak self] in
                self?.getNavigationTitle($0)}
            .bind(to: navigationItem.rx.title)
            .disposed(by: disposeBag)
        
        //table view
        viewModel.reviews
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self](reviews) in
                guard let self = self else { return }
                self.tableView.reloadData()
            })
            .disposed(by: disposeBag)
        
        //loading data
        viewModel.isLoading
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] loading in
                guard let self = self else { return }
                if loading {
                    self.view.hideError()
                    self.tableView.isHidden = false
                }
                else {
                    self.tableView.tableFooterView = nil
                    self.refreshControl.endRefreshing()
                }
            })
            .disposed(by: disposeBag)
        
        //error handling
        viewModel.error
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] error in
                guard let self = self else { return }
                guard let error = error else { return }
                self.tableView.isHidden = true
                self.view.showError(type: error, delegate: self, backgroundColor: self.view.backgroundColor)
            })
            .disposed(by: disposeBag)
        
        //sort
        sortButton.rx.tap
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self](_) in
                guard let self = self else { return }
                self.showSortOrderPicker()
            })
            .disposed(by: disposeBag)
        
        //filter
        filterButton.rx.tap
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self](_) in
                guard let self = self else { return }
                self.showRatingFilter()
            })
            .disposed(by: disposeBag)
    }
    
    //  Refresh control
    private func setupRefreshControl() {
        refreshControl.tintColor = UIColor(named: "gygColor")
        tableView.addSubview(refreshControl)
        //setup refresh action
        refreshControl.rx.controlEvent(.valueChanged)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                guard let self = self else { return }
                if self.refreshControl.isRefreshing {
                    self.viewModel.refreshData()
                }
            })
            .disposed(by: disposeBag)
    }

    //  Navigation title
    private func getNavigationTitle(_ numberOfReviews: Int?) -> String {
        var title = Strings.reviews
        if let numberOfReviews = numberOfReviews {
            title += "(\(numberOfReviews))"
        }
        return title
    }
    
    //  Sort order picker
    private func showSortOrderPicker() {
        let sortOrders = [ReviewsSortOrder.dateDescending, ReviewsSortOrder.dateAscending, ReviewsSortOrder.ratingDescending, ReviewsSortOrder.ratingAscending]
        
        let picker = WMModalPickerView(values: sortOrders.map{$0.rawValue})
        picker.tintColor = UIColor(named: "gygColor")
        picker.selectedValue = viewModel.sortOrder.value.rawValue
        
        picker.presentInWindowWithBlock(callback: { [weak self] madeChoice in
            guard let self = self else { return }
            if madeChoice {
                self.viewModel.setSortOrder(sortOrders[picker.selectedIndex])
            }
        })
    }
    //  Ratings filter picker
    private func showRatingFilter() {
        let ratings = [Int](0...5)
        let picker = WMModalPickerView(values: ratings.map({ (rating) -> String in
            switch rating {
            case 1:
                return String(format: Strings.oneStarRating, "\(rating)")
            case 2...5:
                return String(format: Strings.moreStarsRating, "\(rating)")
            default:
                return Strings.showAllRatings
            }
        }))
        picker.tintColor = UIColor(named: "gygColor")
        picker.selectedIndex = viewModel.ratingFilter.value
        
        picker.presentInWindowWithBlock(callback: { [weak self] madeChoice in
            guard let self = self else { return }
            if madeChoice {
                self.viewModel.setRatingFilter(picker.selectedIndex)
            }
        })
    }
    
}

//  MARK: - UITableViewDelegate & UITableViewDataSource
extension ReviewsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.reviews.value.count > 0 ? viewModel.reviews.value.count : 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.reviews.value.count > 0 ? ReviewTableViewCell.cell(for: viewModel.reviews.value[indexPath.row], in: tableView, at: indexPath) : PreloadTableViewCell.cell(in: tableView, at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if viewModel.reviews.value.count > 2,  indexPath.row >= viewModel.reviews.value.count - 2 {
            viewModel.isLoading.subscribe(onNext: { [weak self](isLoading) in
                guard let self = self else { return }
                if !isLoading, !self.viewModel.stopLoading {
                    self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 40))
                    self.tableView.tableFooterView?.showActivityIndicator()
                    self.viewModel.loadNext()
                }
            }).dispose()
        }
    }
}

//  MARK: - ErrorViewDelegate
extension ReviewsViewController: ErrorViewDelegate {
    func tryAgain() {
        viewModel.refreshData(showPreload: true)
    }
}
