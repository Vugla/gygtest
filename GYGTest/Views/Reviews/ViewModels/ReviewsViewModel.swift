//
//  ReviewsViewModel.swift
//  GYGTest
//
//  Created by Predrag Samardzic on 24/05/2019.
//  Copyright © 2019 Predrag Samardzic. All rights reserved.
//

import RxSwift
import RxCocoa

class ReviewsViewModel {
    //  MARK: - Observables
    let reviews = BehaviorRelay<[Review]>(value: [])
    let numberOfReviews = BehaviorRelay<Int?>(value: nil)
    let error = BehaviorRelay<ErrorType?>(value: nil)
    var isLoading: Observable<Bool> {
        return activityIndicator.asObservable()
    }
    let sortOrder = BehaviorRelay<ReviewsSortOrder>(value: .dateDescending)
    let ratingFilter = BehaviorRelay<Int>(value: 0)
    
    //  MARK: - Properties
    var stopLoading = false
    private final let count = 20
    private var page = 0
    private let activityIndicator = ActivityIndicator()
    private var request: Disposable?
    private let disposeBag = DisposeBag()
    private let path: String
    
    //  MARK: - Life cycle
    init(path: String) {
        self.path = path
        Observable.combineLatest(sortOrder, ratingFilter)
            .distinctUntilChanged{$0.0 == $1.0 && $0.1 == $1.1}
            .subscribe(onNext: { [weak self](_ , _) in
                self?.loadData()
            })
            .disposed(by: disposeBag)
    }
    
    deinit {
        request?.dispose()
    }
    
    //  MARK: - Network
    private func loadData(isRefresh: Bool = false) {
        request?.dispose()
        request = ReactiveClient.shared
            .send(apiRequest: ReviewsRequest<ServerResponse<Review>>(path: path, parameters: [
                "count" : "\(count)",
                "page" : "\(page)",
                "rating": "\(ratingFilter.value)"].merging(sortOrder.value.params, uniquingKeysWith: { (first, _) -> String in
                    return first
                })))
            .trackActivity(activityIndicator)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .utility))
            .subscribe(onNext: { [weak self](response) in
                guard let self = self else { return }
                
                guard let items = response?.data, response?.status == true else {
                    self.stopLoading = true
                    self.error.accept( isRefresh || self.reviews.value.count == 0 ? .noData : nil)
                    return
                }
                self.numberOfReviews.accept(response?.total_reviews_comments)
                self.stopLoading = items.count == 0
                
                let retVal = isRefresh ? items : self.reviews.value + items
                
                self.reviews.accept(retVal)
                self.error.accept(retVal.count == 0 ? .noData : nil)
                }, onError: { [weak self] error in
                    guard let self = self else { return }
                    self.reviews.accept([])
                    self.stopLoading = true
                    switch error._code {
                    case URLError.Code.notConnectedToInternet.rawValue:
                        self.error.accept(.noNet)
                    default:
                        self.error.accept(.server)
                    }
            })
    }
    
    //  MARK: - Utility
    func refreshData(showPreload: Bool = false) {
        if showPreload {
            reviews.accept([])
        }
        stopLoading = false
        page = 0
        loadData(isRefresh: true)
    }
    
    func loadNext() {
        page += 1
        loadData()
    }
    
    func setSortOrder(_ order: ReviewsSortOrder) {
        if order != sortOrder.value {
            reviews.accept([])
            stopLoading = false
            page = 0
            sortOrder.accept(order)
        }
    }
    
    func setRatingFilter(_ rating: Int) {
        let rating = rating < 0 ? 0 : rating > 5 ? 5 : rating
        if rating != ratingFilter.value {
            reviews.accept([])
            stopLoading = false
            page = 0
            ratingFilter.accept(rating)
        }
    }
}
